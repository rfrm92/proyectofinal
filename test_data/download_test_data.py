#!/usr/bin/env python

import os
import sys
import urllib2
import tarfile

try:
    file_directory = os.path.dirname(os.path.abspath(__file__))
    os.chdir(file_directory)
except NameError:
    print "Please, no execute this file from IDLE"
    sys.exit(1)

print "Currently in", os.getcwd()

# Download the file
url = "https://dl.dropboxusercontent.com/u/48646146/test_data.tar.gz"
file_name = url.split('/')[-1]
u = urllib2.urlopen(url)
f = open(file_name, 'wb')
meta = u.info()
file_size = int(meta.getheaders("Content-Length")[0])
print "Downloading: %s Bytes: %s" % (file_name, file_size)

file_size_dl = 0
block_sz = 8192
while True:
    buffer = u.read(block_sz)
    if not buffer:
        break
    file_size_dl += len(buffer)
    f.write(buffer)
    status = r"%10d  [%3.2f%%]" % (file_size_dl, file_size_dl * 100. / file_size)
    status = status + chr(8)*(len(status)+1)
    print status,
f.close()
print "\nDownload complete"

# Decompress
print "Wait, i'm decompressing the file" 
tfile = tarfile.open("test_data.tar.gz", "r:gz")
tfile.extractall('.')
tfile.close()

# Delete file
print "Finally, let's clean"
os.remove("test_data.tar.gz")

print "Ready"
