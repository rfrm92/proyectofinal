#!/usr/bin/env python

import cv2
import sys
import math
import numpy as np

FLANN_INDEX_KDTREE = 1
flann_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
matcher = cv2.FlannBasedMatcher(flann_params, {})

def filter_matches(kp1, kp2, matches, ratio = 0.75):
    mkp1, mkp2 = [], []
    for m in matches:
        if len(m) == 2 and m[0].distance < m[1].distance * ratio:
            m = m[0]
            mkp1.append( kp1[m.queryIdx] )
            mkp2.append( kp2[m.trainIdx] )
    p1 = np.float32([kp.pt for kp in mkp1])
    p2 = np.float32([kp.pt for kp in mkp2])
    kp_pairs = zip(mkp1, mkp2)
    return p1, p2, kp_pairs

def transformImage():
	img = original.copy()
	height, width = img.shape[:2]	
	alpha_radians = math.radians(angle)
	width_big = int(math.ceil((height*math.sin(alpha_radians)+width*math.cos(alpha_radians))))
 	height_big = int(math.ceil((width*math.sin(alpha_radians)+height*math.cos(alpha_radians))))
 	print width_big, height_big
 	rotated_image = np.zeros((height_big, width_big), dtype=np.uint8)
 	print "(%d, %d), (%d, %d)" % ((height_big-height)/2, (height_big-height)/2+img.shape[0], (width_big-width)/2, (width_big-width)/2+img.shape[1])
 	rotated_image[(height_big-height)/2:(height_big-height)/2+img.shape[0], (width_big-width)/2:(width_big-width)/2+img.shape[1]]=img
	image_center = (width_big/2, height_big/2)
	rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1)
	rotated_image = cv2.warpAffine(rotated_image, rot_mat, (width_big, height_big), flags=cv2.INTER_LINEAR)
	
	new_img_kpts, new_img_descriptors = detector.detectAndCompute(rotated_image, None)
	raw_matches = matcher.knnMatch(descriptors, trainDescriptors = new_img_descriptors, k = 2) #2
	p1, p2, kp_pairs = filter_matches(kpts, new_img_kpts, raw_matches)

	for kpt in new_img_kpts:
	    x1, y1 = [int(c) for c in kpt.pt]
	    cv2.circle(rotated_image, (x1, y1), 2, (0, 255, 0), -1)
	cv2.imshow("transformed", rotated_image)
	

def rotateImage(new_angle):
	global angle
	angle = new_angle
	transformImage()
	
def scaleImage(new_scale):
	global scale
	scale = new_scale
	transformImage()
	

try:
	original = cv2.imread(sys.argv[1])
except IndexError:
	original = cv2.imread('2.png')
original = cv2.cvtColor(original, cv2.cv.CV_RGB2GRAY)
print original.dtype

angle = 0 
scale = 200
detector = cv2.SIFT()
with_kp = original.copy()
kpts, descriptors = detector.detectAndCompute(original, None)
for kpt in kpts:
    x1, y1 = [int(c) for c in kpt.pt]
    cv2.circle(with_kp, (x1, y1), 2, (0, 255, 0), -1)
cv2.imshow("img", with_kp)

cv2.cv.CreateTrackbar("angle", "img", 0, 89, rotateImage)
cv2.cv.CreateTrackbar("scale", "img", 100, 200, scaleImage)

cv2.namedWindow("transformed")
transformImage()

cv2.waitKey()
cv2.destroyAllWindows()
