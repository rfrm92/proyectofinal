import subprocess
import numpy as np

class VideoSink(object):
    def __init__(self, size, filename="output", rate=10, byteorder="bgra"):        
        self.size = size
        self.alpha_channel = 255*np.ones(size, dtype=np.uint8)
        cmdstring  = ('mencoder',
                '/dev/stdin',
                '-demuxer', 'rawvideo',
                '-rawvideo', 'w=%i:h=%i'%size[::-1]+":fps=%i:format=%s"%(rate,byteorder),
                '-o', filename+'.avi',
                '-ovc', 'lavc')
        self.p = subprocess.Popen(cmdstring, stdin=subprocess.PIPE, shell=False)

    def write(self, image):            
        assert image.shape[:2] == self.size
        image = np.dstack([image, self.alpha_channel])
        self.p.stdin.write(image.tostring())

    def close(self):
        self.p.stdin.close()
