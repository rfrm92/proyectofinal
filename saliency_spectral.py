#!/usr/bin/env python

import sys
import random
import time
import cv2.cv
import cv2 as cv
import achanta

from scipy import ndimage, fftpack
from numpy import log, abs, angle, exp, max, min, transpose, mean, sqrt

resz = 64
sigma = 8
threshv = 0.5
frame_skip = 1

def timeit(f):
    def inner(*args):
        ti=time.clock()
        retval=f(*args)
        print time.clock()-ti
        return retval
    return inner

def mat2gray(im):
    im_max=max(im)
    im_min=min(im)
    return (im-im_max)/(im_max-im_min)+1

@timeit
def get_saliency_map(original): 
    saturation = cv.cvtColor(original, cv2.cv.CV_RGB2HSV)[:,:,1]
    im = ndimage.filters.gaussian_filter(saturation.astype('float32'), 3)   
    im = cv.resize(im, (resz, resz), interpolation=cv.INTER_AREA)
    myfft = fftpack.fft2(im)
    logAmplitude = log(abs(myfft))
    phase = angle(myfft)
    smoothed = ndimage.filters.uniform_filter(logAmplitude, 5)
    residual = logAmplitude - smoothed
    sm = abs(fftpack.ifft2(exp(residual+1j*phase)))**2
    sm = mat2gray(ndimage.filters.gaussian_filter(sm, sigma))   

    t, sm = cv.threshold((255*sm).astype('uint8'), 0, 255, cv2.cv.CV_THRESH_BINARY | cv2.cv.CV_THRESH_OTSU)
    # threshval, sm = cv.threshold(sm, threshv, 1, cv.THRESH_BINARY)
    sm = cv.resize(sm, (original.shape[1], original.shape[0]))
    return sm

@timeit
def bound_salient_regions(original):
    copy = original.copy()
    im = cv.cvtColor(original, cv.COLOR_RGB2GRAY).astype('float32')
    im = cv.resize(im, (resz, resz), interpolation=cv.INTER_AREA)
    myfft = fftpack.fft2(im)
    logAmplitude = log(abs(myfft))
    phase = angle(myfft)
    smoothed = ndimage.filters.uniform_filter(logAmplitude, 5)
    residual = logAmplitude - smoothed
    sm = abs(fftpack.ifft2(exp(residual+1j*phase)))**2
    sm = ndimage.filters.gaussian_filter(sm, sigma)
    sm = mat2gray(sm)
    t, sm = cv.threshold((255*sm).astype('uint8'), 0, 255, cv2.cv.CV_THRESH_BINARY | cv2.cv.CV_THRESH_OTSU)
    sm = cv.resize(sm, (original.shape[1], original.shape[0])).astype('uint8')
    
    contours, hierarchy = cv.findContours(sm, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
    for points  in contours:
        xi, yi, w, h = cv.boundingRect(points)
        cv.rectangle(copy, (xi, yi), (xi+w, yi+h), (255, 255, 255), 2)

    return copy

@timeit
def salient_intensity(original):
    copy = original.copy()
    im = cv.cvtColor(original, cv.CV_RGB2HSV)
    im = cv.resize(im, (resz, resz), interpolation=cv.INTER_AREA)/255.0
    myfft = fftpack.fft2(im)
    logAmplitude = log(abs(myfft))
    phase = angle(myfft)
    smoothed = ndimage.filters.uniform_filter(logAmplitude, 5)
    residual = logAmplitude - smoothed
    sm = abs(fftpack.ifft2(exp(residual+1j*phase)))**2
    sm = ndimage.filters.gaussian_filter(sm, sigma)
    sm = mat2gray(sm)
    threshval, sm = cv.threshold(sm, threshv, 1, cv.THRESH_BINARY)
    sm = cv.resize(sm, (original.shape[1], original.shape[0]))
    copy[:,:,0]*=sm
    copy[:,:,1]*=sm
    copy[:,:,2]*=sm
    return copy

def change_threshold(thresh_val):
    global threshv
    threshv=thresh_val/100.0    

def change_resz(res_size):
    global resz
    resz = res_size 

def change_sigma(sigmav):
    global sigma
    sigma = sigmav/10.0 

def change_frame_skip(new_frame_skip):
    global frame_skip
    frame_skip = new_frame_skip

def put_frame_skip(image, frame_skip):
    location=(0,30)    
    fontface=cv.FONT_HERSHEY_PLAIN  
    fontscale=2.0  
    color=(255,190,0)
    cv.putText(image, unicode(frame_skip)+"x", location, fontface, fontscale, color, 3)

original = cv.imread('./data/leaf.jpg')
im = achanta.my_resize(original)
saliency = get_saliency_map(im)
cv.imshow("im", saliency)
cv.waitKey()

# cv.namedWindow("im")
# cv2.cv.CreateTrackbar("threshold", "im", int(threshv*100), 100, change_threshold) 
# cv2.cv.CreateTrackbar("resize", "im", resz, 300, change_resz) 
# cv2.cv.CreateTrackbar("sigma", "im", 10*sigma, 100, change_sigma) 
# cv2.cv.CreateTrackbar("frame skip", "im", frame_skip, 10, change_frame_skip) 

# is_playing = True
# vc = cv.VideoCapture()

# try:
#   vc.open(sys.argv[1])
# except:
#   vc.open('data/v3.mp4')

# retval, original = vc.read()
# im = achanta.my_resize(original)
# rows, cols = im.shape[:2]
# sc = achanta.SaliencyCalculator(rows, cols)


# while True:
    # if is_playing:
    #   for _ in range(frame_skip):
    #       retval, original = vc.read()


    # selection, mask = sc.get_salient_regions_bounded(im.astype('float32')/255.)

    

    # key=cv.waitKey(1) & 255
    # if key==112:
    #   is_playing = not is_playing
    # elif key==115:
    #   cv.imwrite("out.png", original)
    # elif key==27:
    #   break

cv.destroyAllWindows()
