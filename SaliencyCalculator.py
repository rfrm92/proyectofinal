#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import cv2
import math
import pdb
import atexit
import pickle
import random
import collections
import numpy as np
import itertools as it
from numbapro import jit
from VideoSink import VideoSink
from sift.find_obj import explore_match
from multiprocessing.pool import ThreadPool

region_memory=[]
detector = cv2.SIFT()
pool=ThreadPool(processes = cv2.getNumberOfCPUs())

FLANN_INDEX_KDTREE = 1
MEMFILE_PATH = os.path.join("memory", "memory.dat")
matcher = cv2.FlannBasedMatcher(dict(algorithm=FLANN_INDEX_KDTREE, trees=5), {})

def timeit(f):
    import time
    def inner(*args):
        ti=time.time()
        retval=f(*args)
        print "Llamada a", f.__name__, "tomó:", time.time()-ti
        return retval
    return inner

def filter_matches(kp1, kp2, matches, ratio=0.75):
    mkp1, mkp2 = [], []
    for m in matches:
        try:
            if m[0].distance < m[1].distance * ratio:
                m = m[0]
                mkp1.append( kp1[m.queryIdx] )
                mkp2.append( kp2[m.trainIdx] )
        except IndexError:
            continue
    p1 = np.float32([kp.pt for kp in mkp1])
    p2 = np.float32([kp.pt for kp in mkp2])
    kp_pairs = zip(mkp1, mkp2)
    return p1, p2, kp_pairs

def load_memory():
    #pdb.set_trace()
    global region_memory
    try:
        with open(MEMFILE_PATH, 'r') as f:
            region_memory = pickle.load(f)
            for region in region_memory:
                region_keypoints = []
                for kpt in region.keypoints:
                    x, y, size, angle, response, octave, class_id = kpt               
                    region_keypoints.append(cv2.KeyPoint(x, y, size, angle, \
                        response, octave, class_id))
                region.keypoints = region_keypoints
    except IOError:
        print "There's no memory file"        
        
def save_memory():
    # pdb.set_trace()
    for region in region_memory:
        region_keypoints = []
        for kpt in region.keypoints:
            x, y = kpt.pt
            kpt = (x, y, kpt.size, kpt.angle, kpt.response, kpt.octave, kpt.class_id)
            region_keypoints.append(kpt)
        region.keypoints = region_keypoints
            
    with open(MEMFILE_PATH, 'w') as f:
        pickle.dump(region_memory, f)

def affine_skew(tilt, phi, img, mask=None):
    '''
    affine_skew(tilt, phi, img, mask=None) -> skew_img, skew_mask, Ai

    Ai - is an affine transform matrix from skew_img to img
    '''
    h, w = img.shape[:2]
    if mask is None:
        mask = np.zeros((h, w), np.uint8)
        mask[:] = 255
    A = np.float32([[1, 0, 0], [0, 1, 0]])
    if phi != 0.0:
        phi = np.deg2rad(phi)
        s, c = np.sin(phi), np.cos(phi)
        A = np.float32([[c,-s], [ s, c]])
        corners = [[0, 0], [w, 0], [w, h], [0, h]]
        tcorners = np.int32( np.dot(corners, A.T) )
        x, y, w, h = cv2.boundingRect(tcorners.reshape(1,-1,2))
        A = np.hstack([A, [[-x], [-y]]])
        img = cv2.warpAffine(img, A, (w, h), flags=cv2.INTER_LINEAR, borderMode=cv2.BORDER_REPLICATE)
    if tilt != 1.0:
        s = 0.8*np.sqrt(tilt*tilt-1)
        img = cv2.GaussianBlur(img, (0, 0), sigmaX=s, sigmaY=0.01)
        img = cv2.resize(img, (0, 0), fx=1.0/tilt, fy=1.0, interpolation=cv2.INTER_NEAREST)
        A[0] /= tilt
    if phi != 0.0 or tilt != 1.0:
        h, w = img.shape[:2]
        mask = cv2.warpAffine(mask, A, (w, h), flags=cv2.INTER_NEAREST)
    Ai = cv2.invertAffineTransform(A)
    return img, mask, Ai

def affine_detect(detector, img, mask=None, pool=None):
    '''
    affine_detect(detector, img, mask=None, pool=None) -> keypoints, descrs

    Apply a set of affine transormations to the image, detect keypoints and
    reproject them into initial image coordinates.
    See http://www.ipol.im/pub/algo/my_affine_sift/ for the details.

    ThreadPool object may be passed to speedup the computation.
    '''
    params = [(1.0, 0.0)]
    for t in 2**(0.5*np.arange(1,6)):
        for phi in np.arange(0, 180, 72.0 / t):
            params.append((t, phi))

    def f(p):
        t, phi = p
        timg, tmask, Ai = affine_skew(t, phi, img)
        keypoints, descrs = detector.detectAndCompute(timg, tmask)
        for kp in keypoints:
            x, y = kp.pt
            kp.pt = tuple( np.dot(Ai, (x, y, 1)) )
        if descrs is None:
            descrs = []
        return keypoints, descrs
    keypoints, descrs = [], []
    if pool is None:
        ires = it.imap(f, params)
    else:
        ires = pool.imap(f, params)
    for i, (k, d) in enumerate(ires):
        # print 'affine sampling: %d / %d\r' % (i+1, len(params)),
        keypoints.extend(k)
        descrs.extend(d)
    # print
    return keypoints, np.array(descrs)

def show_keypoints(win_name, img, keypoints):
    img_copy = img.copy()
    for kpt in keypoints:
        x1, y1 = [int(c) for c in kpt.pt]
        cv2.circle(img_copy, (x1, y1), 2, (0, 255, 0), -1)
    cv2.imshow(win_name, img_copy)
    cv2.waitKey()
    cv2.destroyAllWindows()

def get_similar_regions(img, candidate, candidate_img=None):
    similar_regions = []
    for region in region_memory:
        raw_matches = matcher.knnMatch(candidate.descriptors, trainDescriptors=region.descriptors, k=2)
        p1, p2, kp_pairs = filter_matches(candidate.keypoints, region.keypoints, raw_matches)
        if len(p1)>=4:
            H, status = cv2.findHomography(p1, p2, cv2.RANSAC, 3.0)
            inliers_matched_ratio = np.sum(status)/len(status)
            if inliers_matched_ratio>0.7:
                similar_regions.append((len(status), region))
                if not (candidate_img is None):

                    # if len(candidate.keypoints)>len(region.keypoints):
                    #     region.keypoints = candidate.keypoints
                    #     region.descriptors = candidate.descriptors
                    #     region_image = 

                    region_image = cv2.imread(os.path.join("memory", str(region.id)+".png"), 0)

                    if len(candidate_img.shape)!=2:                    
                        candidate_img = cv2.cvtColor(candidate_img, cv2.cv.CV_BGR2GRAY)

                    show_keypoints("candidate keypoints", candidate_img, candidate.keypoints)
                    show_keypoints("in database image", region_image, region.keypoints)

                    print 'Region %d: %d / %d  inliers/matched' % (region.id, np.sum(status), len(status))
                    vis = explore_match("Explore match", candidate_img, region_image, kp_pairs, status, H)
                    cv2.waitKey()
                    cv2.destroyAllWindows()

                    print "vis closed"

    return similar_regions

def add_region(img, candidate):
    similar_regions = get_similar_regions(img, candidate)
    if not similar_regions:
        candidate.set_id(len(region_memory))
        xi, yi, w, h = candidate.get_bounding_rect()
        cv2.imwrite(os.path.join("memory", str(candidate.id)+".png"), img[yi:yi+h, xi:xi+w])
        region_memory.append(candidate)
        return True
    return False

def remember(img, candidate):
    xi, yi, w, h = candidate.get_bounding_rect()
    candidate_image = img[yi:yi+h, xi:xi+w]
    similar_regions = get_similar_regions(img, candidate, candidate_image)
    if similar_regions:
        similar_regions = sorted(similar_regions) 
        return similar_regions[-1][-1]
    return None

def add_region_or_get_detected(img, candidate):
    detected_regions = get_similar_regions(img, candidate)
    
    # Filter out important regions
    if detected_regions:
        detected_regions = sorted(detected_regions)  
        return detected_regions[-1][-1]
    else:        
        candidate.set_id(len(region_memory))
        xi, yi, w, h = candidate.get_bounding_rect()
        cv2.imwrite(os.path.join("memory", str(candidate.id)+".png"), img[yi:yi+h, xi:xi+w])
        region_memory.append(candidate)
        return None

def normalize(im):
    im_max=np.max(im)
    im_min=np.min(im)   
    return (im-im_max)/(im_max-im_min)+1

def my_resize(im):
    rows, cols = im.shape[:2]   
    a = np.sqrt(120000./(rows*cols))    
    rows, cols = int(a*rows), int(a*cols)   
    return cv2.resize(im, (cols, rows), interpolation=cv2.INTER_AREA)

#@timeit
@jit("void(float_[:,:],float_[:,:],float_[:,:],float_[:,:],float_[:,:],float_[:,:],float_[:,:])")
def get_mean(result, smoothed_l, smoothed_a, smoothed_b, integral_l, integral_a, integral_b):
    rows, cols = smoothed_l.shape
    for y in range(rows):
        yoff = min(y, rows-y)
        y1 = max(y-yoff,0)
        y2 = min(y+yoff, rows-2)+1
        for x in range(cols):
            xoff = min(x, cols-x)
            x1 = max(x-xoff, 0)
            x2 = min(x+xoff, cols-2)+1
            a = (x2-x1)*(y2-y1)
            mean_l = (integral_l[y2, x2] + integral_l[y1, x1] - \
                        integral_l[y1, x2] - integral_l[y2, x1])/a
            mean_a = (integral_a[y2, x2] + integral_a[y1, x1] - \
                        integral_a[y1, x2] - integral_a[y2, x1])/a

            mean_b = (integral_b[y2, x2] + integral_b[y1, x1] - \
                        integral_b[y1, x2] - integral_b[y2, x1])/a
            result[y, x] = (smoothed_l[y, x]-mean_l)**2+\
                           (smoothed_a[y, x]-mean_a)**2+\
                           (smoothed_b[y, x]-mean_b)**2

def point_inside_contour(point, contour):        
    return cv2.pointPolygonTest(contour, point, False)>=0

class Region(object):
    def __init__(self, contour, keypoints, descriptors):                
        self.contour = contour
        self.keypoints = keypoints
        self.bounding_rect = cv2.boundingRect(contour)
        self.descriptors = np.vstack(descriptors)

    def set_id(self, id):
        self.id = id

    def get_bounding_rect(self):
        return self.bounding_rect


class SaliencyCalculator(object):
    def __init__(self, rows, cols):
        self.rows = rows
        self.cols = cols
        self.l = np.empty((rows, cols), dtype='float32')
        self.a = np.empty((rows, cols), dtype='float32')
        self.b = np.empty((rows, cols), dtype='float32')
        self.smoothed_l = np.empty((rows, cols), dtype='float32')
        self.smoothed_a = np.empty((rows, cols), dtype='float32')
        self.smoothed_b = np.empty((rows, cols), dtype='float32')
        self.integral_l = np.empty((rows+1, cols+1), dtype='float32')
        self.integral_a = np.empty((rows+1, cols+1), dtype='float32')
        self.integral_b = np.empty((rows+1, cols+1), dtype='float32')
        self.sm = np.empty((rows, cols), dtype='float32')        
        self.lab = np.empty((rows, cols, 3), dtype='float32')   
        self.k1=cv2.getGaussianKernel(3, 0)
        self.k2=self.k1.T

    def _blur_lab(self):
        cv2.filter2D(self.lab, -1, self.k1, self.lab)
        cv2.filter2D(self.lab, -1, self.k2, self.lab)
    
    def get_saliency_map(self, img):
        """Calculate the saliency map using: "Saliency Detection using Maximum 
        Symmetric Surround" algorithm 

        args:
            img(uint8): A uint8 image to get the saliency map of.

        Returns a uint8 grayscale image whose pixel values indicate the saliency of
        that pixel

        """
        assert img.dtype == np.uint8

        img = img.astype('float32') / 255.
        cv2.cvtColor(img, cv2.cv.CV_RGB2Lab, self.lab)
        cv2.split(self.lab, [self.l, self.a, self.b])
        self._blur_lab()
        cv2.split(self.lab, [self.smoothed_l, self.smoothed_a, self.smoothed_b])
        cv2.integral(self.l, self.integral_l, sdepth=cv2.CV_32F)
        cv2.integral(self.a, self.integral_a, sdepth=cv2.CV_32F)
        cv2.integral(self.b, self.integral_b, sdepth=cv2.CV_32F)
        get_mean(self.sm, self.smoothed_l, self.smoothed_a, self.smoothed_b, \
            self.integral_l, self.integral_a, self.integral_b)
        sm = (255*normalize(self.sm)).astype('uint8')        
        return sm

    # @timeit
    def get_salient_regions_mask_and_contours(self, img):
        """Return the contours of the salient regions and a mask
        where 1 is salient region and 0 is background.

        args:
            img(uint8): A uint8 image to get mask and bounding rectangle.
        returns:
            contours: The contours of the salient regions.
            mask: a mask where 1 is salient region and 0 is background.

        """
        saliency_map = self.get_saliency_map(img)
        threshold_value, mask = cv2.threshold(saliency_map, 0, 1,
            cv2.cv.CV_THRESH_BINARY | cv2.cv.CV_THRESH_OTSU)       
        mask = (cv2.GC_PR_BGD*(mask==0)+cv2.GC_PR_FGD*(mask==1)).astype('uint8')        
        bgdModel, fgdModel = np.zeros((1,65), np.float64), np.zeros((1,65), np.float64)
        cv2.grabCut(img, mask, None, bgdModel, fgdModel, 5, cv2.GC_INIT_WITH_MASK)
        mask = np.where((mask==1) + (mask==3), 255, 0).astype('uint8')
        contours, hierarchy = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, 
            cv2.CHAIN_APPROX_NONE)
        
        copy = img.copy()
        cv2.drawContours(copy, contours, -1, (0, 255, 0), 1)
        cv2.imshow("selected objects", copy)

        return contours, mask

    def get_salient_regions(self, img):
        original = img.copy()
        contours, mask = self.get_salient_regions_mask_and_contours(img)
        kpts, descriptors = affine_detect(detector, img, mask=mask, pool=pool)
        # kpts, descriptors = detector.detectAndCompute(img, mask=mask)
        
        contours_keypoints = [[] for contour in contours]
        contours_keypoints_descriptors = [[] for contour in contours]
        for kpt, descriptor in zip(kpts, descriptors):
            for index, contour in enumerate(contours):
                if point_inside_contour(kpt.pt, contour):
                    contours_keypoints[index].append(kpt)
                    contours_keypoints_descriptors[index].append(descriptor)
                    break

        detected_regions = []
        for contour, contour_keypoints, contour_keypoints_descriptors in \
            zip(contours, contours_keypoints, contours_keypoints_descriptors):           
            if len(contour_keypoints)>=50:
                region = Region(contour=contour, keypoints=contour_keypoints, \
                     descriptors=contour_keypoints_descriptors)
                xi, yi, w, h = region.get_bounding_rect()  

                for kpt in contour_keypoints:
                    kpt.pt = (kpt.pt[0]-xi, kpt.pt[1]-yi)

                sub_rect = img[yi:yi+h, xi:xi+w]
                show_keypoints("salient region", sub_rect, contour_keypoints)

                detected_regions.append(region)

        return detected_regions

    # @timeit
    def learn(self, img):
        original = img.copy()
        detected_regions = self.get_salient_regions(img)

        for region in detected_regions:
            xi, yi, w, h = region.get_bounding_rect()
            center = (xi+w/2, yi+h/2)

            if add_region(original, region):
                cv2.rectangle(img, (xi, yi), (xi+w, yi+h), (0,255,0), 1)
                cv2.putText(img, "New", center, cv2.FONT_HERSHEY_PLAIN, 2, (255,0,0), 2)

        return img

    def redetect(self, img):
        original = img.copy()
        detected_regions = self.get_salient_regions(img)

        for region in detected_regions:
            xi, yi, w, h = region.get_bounding_rect()
            center=(xi+w/2, yi+h/2)
            remembered_region = remember(original, region)
            if not (remembered_region is None):
                cv2.rectangle(img, (xi, yi), (xi+w, yi+h), (0,255,0), 1)
                cv2.putText(img, str(remembered_region.id), center, cv2.FONT_HERSHEY_PLAIN, 2, (255,0,0), 2)

        return img

    def learn_and_detect(self, img):
        copy = img.copy()
        detected_regions = self.get_salient_regions(img)

        for region in detected_regions:
            xi, yi, w, h = region.get_bounding_rect()
            center=(xi+w/2, yi+h/2)
            detected_region = add_region_or_get_detected(copy, region)
            if detected_region:
                # for kpt in region.keypoints:
                #     x1, y1 = [int(c) for c in kpt.pt]
                #     cv2.circle(img, (x1, y1), 2, (0,255,0), -1)
                cv2.rectangle(img, (xi, yi), (xi+w, yi+h), (0,255,0), 1)
                cv2.putText(img, str(detected_region.id), center, cv2.FONT_HERSHEY_PLAIN, 2, (255,0,0), 2)
            else:                
                # for kpt in region.keypoints:
                #     x1, y1 = [int(c) for c in kpt.pt]
                #     cv2.circle(img, (x1, y1), 2, (255,0,0), -1)
                cv2.rectangle(img, (xi, yi), (xi+w, yi+h), (255,0,0), 1)
                cv2.putText(img, "New", center, cv2.FONT_HERSHEY_PLAIN, 2, (0,0,255), 2)
        return img

def learn_img_num(num):
    vc = cv2.VideoCapture()
    vc.open('test_data/videos/v3.mp4')
    retval, original = vc.read()
    im = my_resize(original)
    rows, cols = im.shape[:2]
    sc = SaliencyCalculator(rows, cols)
    for _ in range(190+num):
        retval, original = vc.read()
    im = my_resize(original)
    sc.learn(im)

def redetect_img_num(num):
    vc = cv2.VideoCapture()
    vc.open('test_data/videos/v3.mp4')
    retval, original = vc.read()
    im = my_resize(original)
    rows, cols = im.shape[:2]
    sc = SaliencyCalculator(rows, cols)
    for _ in range(190+num):
        retval, original = vc.read()
    im = my_resize(original)

    return sc.redetect(im)

def main():
    load_memory()
    
    vc = cv2.VideoCapture()    
    vc.open('test_data/videos/v3.mp4')
    retval, original = vc.read()
    im = my_resize(original)
    rows, cols = im.shape[:2]
    sc = SaliencyCalculator(rows, cols)

    frame_id = 1122
    for _ in range(190+frame_id):
        retval, original = vc.read()

    play = True
    save_count = 0
    while play:
        retval, original = vc.read()
        if original != None:
            im = my_resize(original)
            objects = sc.learn_and_detect(im)           
            cv2.imshow("objects", objects)
            if (cv2.waitKey(1) & 255) == 27:
                play = False
            print "Processed image", os.path.join("video_frames", "%04d.png"%frame_id)
            cv2.imwrite(os.path.join("video_frames", "%04d.png"%frame_id), objects)
            frame_id += 1            
            
            save_count += 1
            if save_count > 20:
                save_memory()
                load_memory()
                print "Last save in frame", frame_id
                save_count = 0
        else:
            print "Finished processing"
            break        
    cv2.destroyAllWindows()

def main2():
    load_memory()
    frame_id = 0
    # learn_img_num(1)
    out = redetect_img_num(100)

    while True:
        cv2.imshow("redetection", out)
        key=cv2.waitKey(1) & 255
        if key==27:
          break
    cv2.destroyAllWindows()

atexit.register(save_memory)
    
if __name__ == "__main__":
    try:
        main2()
    except Exception, e:
        raise e
