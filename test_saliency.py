#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import cv2
import os.path
import numpy as np
from SaliencyCalculator import SaliencyCalculator

test_data = []
for folder_index in range(10):
    file_name = os.path.abspath(os.path.join('test_data', 'UserData', '%d_data.txt'%folder_index))
    with open(file_name) as f:        
        nimages = int(f.readline())
        for i in range(nimages):
            f.readline()
            image_path = f.readline().strip().replace("\\", "/")
            f.readline()
            rectangles =  [[int(v) for v in rect_data.strip().split(" ")]
                                   for rect_data in f.readline().strip().split(";") if rect_data]
            test_data.append([image_path, rectangles])

c = 0
sum_recall = 0.
sum_precision = 0.
for image_path, rectangles_coordinates in test_data:
    c+=1
    print "-"*30+str(c)+"-"*30
    original = cv2.imread(os.path.abspath(os.path.join('test_data', 'Image', image_path)))
    rows, cols = original.shape[:2]
    im = original.copy()

    m=0
    saliency_probability = np.zeros(im.shape[:2])
    for xi, yi, xf, yf in rectangles_coordinates:
        cv2.rectangle(im, (xi, yi), (xf, yf), (255, 255, 255), 2)
        saliency_probability[yi:yf, xi:xf]+=1.
        m+=1.
    saliency_probability/=m

    sc = SaliencyCalculator(rows, cols)
    saliency_mask = np.zeros(im.shape[:2])
    contours, mask = sc.get_salient_regions_mask_and_contours(original)

    algorithm_result = original.copy()
    cv2.drawContours(algorithm_result, contours, -1, (0,255,0), 2)
    for contour in contours:
        xi, yi, w, h = cv2.boundingRect(contour)
        cv2.rectangle(algorithm_result, (xi, yi), (xi+w, yi+h), (255, 255, 255), 2)
        saliency_mask[yi:yi+h, xi:xi+w]=1.

    comp = np.hstack([im, algorithm_result])
    cv2.imwrite(os.path.join("saliency_results", str(c)+".png"), comp)

    alpha = 0.5
    presicion = np.sum(saliency_probability*saliency_mask)/np.sum(saliency_mask)
    recall = np.sum(saliency_probability*saliency_mask)/np.sum(saliency_probability)
    fmeasure = (1+alpha)*presicion*recall/(alpha*presicion+recall)

    if not (np.isnan(presicion) or np.isnan(recall) or np.isnan(fmeasure)):
        sum_precision += presicion
        sum_recall += recall
        print "Precision: %.3f\nRecall: %.3f\nF-Measure: %.3f\n"%(presicion, recall, fmeasure)
    else:
        print "Precision: 0\nRecall: 0\nF-Measure: 0\n"

    algorithm_recall = sum_recall/c
    algorith_precision = sum_precision/c
    algorithm_fmeasure = (1+alpha)*algorith_precision*algorithm_recall/(alpha*algorith_precision+algorithm_recall)
    print "Algorithm precision: %.3f\nAlgorithm recall: %.3f\nAlgoritm f-Measure: %.3f\n"%(algorith_precision, algorithm_recall, algorithm_fmeasure)

cv2.destroyAllWindows()
