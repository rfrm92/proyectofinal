#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import sys
import cv2
import math
import numpy as np
from numba import jit, autojit, prange

def timeit(f):
	def inner(*args):
		ti=time.clock()
		retval=f(*args)
		print "Llamada a", f.__name__, "tomó: ", time.clock()-ti
		return retval
	return inner

@timeit
def normalize(im):
	im_max=np.max(im)
	im_min=np.min(im)	
	return (im-im_max)/(im_max-im_min)+1

def my_resize(im):
	rows, cols = im.shape[:2]	
	a = np.sqrt(120000./(rows*cols))	
	rows, cols = int(a*rows), int(a*cols)	
	return cv2.resize(im, (cols, rows), interpolation=cv2.INTER_AREA)	

cv2.cvtColor = timeit(cv2.cvtColor)
cv2.grabCut = timeit(cv2.grabCut)

@timeit
@jit("void(float_[:,:],uint8[:,:],uint8[:,:],uint8[:,:],int32[:,:],int32[:,:],int32[:,:])")
def get_mean(result, smoothed_l, smoothed_a, smoothed_b, integral_l, integral_a, integral_b):
	rows, cols = smoothed_l.shape	
	for y in range(rows):
		yoff = min(y, rows-y)
		y1 = max(y-yoff,0)
		y2 = min(y+yoff, rows-2)
		for x in range(cols):					
			xoff = min(x, cols-x)
			x1 = max(x-xoff, 0)
			x2 = min(x+xoff, cols-2)
			a = (x2-x1+1.)*(y2-y1+1.)
			mean_l = (integral_l[y2+1, x2+1] + integral_l[y1, x1] - 
						integral_l[y1, x2+1] - integral_l[y2+1, x1])/a	
			mean_a = (integral_a[y2+1, x2+1] + integral_a[y1, x1] - 
						integral_a[y1, x2+1] - integral_a[y2+1, x1])/a
			mean_b = (integral_b[y2+1, x2+1] + integral_b[y1, x1] - 
						integral_b[y1, x2+1] - integral_b[y2+1, x1])/a
			result[y, x] =(mean_l-smoothed_l[y, x])**2+\
						   (mean_a-smoothed_a[y, x])**2+\
						   (mean_b-smoothed_b[y, x])**2


class SaliencyCalculator(object):
	def __init__(self, rows, cols, ksize=3):
		self.rows = rows
		self.cols = cols
		self.l = np.zeros((rows, cols), dtype='uint8')
		self.a = np.zeros((rows, cols), dtype='uint8')
		self.b = np.zeros((rows, cols), dtype='uint8')
		self.smoothed_l = np.zeros((rows, cols), dtype='uint8')
		self.smoothed_a = np.zeros((rows, cols), dtype='uint8')
		self.smoothed_b = np.zeros((rows, cols), dtype='uint8')
		self.sm = np.zeros((rows, cols), dtype='float32')
		self.lab = np.zeros((rows, cols, 3), dtype='uint8')
		self.integral_l = np.zeros((rows+1, cols+1), dtype='int32')
		self.integral_a = np.zeros((rows+1, cols+1), dtype='int32')
		self.integral_b = np.zeros((rows+1, cols+1), dtype='int32')		
		self.k1=cv2.getGaussianKernel(ksize, 0)
		self.k2=self.k1.T

	def _blur_lab(self):
		cv2.filter2D(self.lab, -1, self.k1, self.lab)
		cv2.filter2D(self.lab, -1, self.k2, self.lab)

	@timeit
	def get_saliency_map(self, im):
		cv2.cvtColor(im, cv2.cv.CV_RGB2Lab, self.lab)
		cv2.split(self.lab, [self.l, self.a, self.b])
		self._blur_lab()
		cv2.split(self.lab, [self.smoothed_l, self.smoothed_a, self.smoothed_b])	
		cv2.integral(self.l, self.integral_l)
		cv2.integral(self.a, self.integral_a)
		cv2.integral(self.b, self.integral_b)

		get_mean(self.sm, self.smoothed_l, self.smoothed_a, self.smoothed_b, \
			self.integral_l, self.integral_a, self.integral_b)

		return (255*normalize(self.sm)).astype('uint8')

	# @timeit
	# def get_saliency_map2(self, im):
	# 	cv2.cvtColor(im, cv2.cv.CV_RGB2Lab, self.lab)
	# 	cv2.split(self.lab, [self.l, self.a, self.b])
	# 	lm = np.mean(self.l)
	# 	am = np.mean(self.a)
	# 	bm = np.mean(self.b)
	# 	self._blur_lab()
	# 	cv2.split(self.lab, [self.l, self.a, self.b])
	# 	return (255*normalize((self.l-lm)**2+(self.a-am)**2+(self.b-bm)**2)).astype('uint8')
	
	@timeit
	def select_salient_regions(self, im):
		s1 = sc.get_saliency_map(im)	
		t, mask = cv2.threshold(s1, 0, 1, cv2.cv.CV_THRESH_BINARY | cv2.cv.CV_THRESH_OTSU)
		mask = (cv2.GC_PR_BGD*(mask==0)+cv2.GC_PR_FGD*(mask==1)).astype('uint8')
		bgdModel = np.zeros((1,65), np.float64)
		fgdModel = np.zeros((1,65), np.float64)	
		cv2.grabCut(im, mask, None, bgdModel, fgdModel, 5, cv2.GC_INIT_WITH_MASK)
		mask2 = np.where((mask==1) + (mask==3), 255, 0).astype('uint8')
		selection = cv2.bitwise_and(im, im, mask=mask2)
		return selection, mask2

	@timeit
	def get_salient_regions_bounded(self, im):
		selection, mask = self.select_salient_regions(im)
		contours, hierarchy = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
		
		# return selection, mask
		fontface=cv2.FONT_HERSHEY_PLAIN  
		fontscale=1.0  
		color=(255, 189, 0)
		for i, contour in enumerate(contours):		
			length = cv2.arcLength(contour, True)
			area = cv2.contourArea(contour)
			
			if length and area:
				# factor = area/length
				factor = area**2/(length*self.rows*self.cols)			

				#print "Area maxima: %d\nPerimetro: %.2f\
				#	\nArea: %.2f\nRatio: %.3f\n"%(self.cols*self.rows, length, area, factor)				
				xi, yi, w, h = cv2.boundingRect(contour)
				centerx=xi+w/2
				centery=yi+h/2			
				cv2.rectangle(selection, (xi, yi), (xi+w, yi+h), (255, 255, 255), 2)
				location=(centerx, centery)
				cv2.putText(selection, "%.3f"%factor, location, fontface, fontscale, color, 1)
				cv2.drawContours(selection, contours, i, (0, 255, 0), 1)

		return selection, mask

if __name__ == "__main__":
	# Variables
	if len(sys.argv)>1:
		im = cv2.imread(sys.argv[1]).astype('uint8')
	else:
		# im = cv2.imread("data/out.png").astype('uint8')
		im = cv2.imread("data/peluche.jpg").astype('uint8')

	im = my_resize(im)
	rows, cols = im.shape[:2]	
	
	sc = SaliencyCalculator(rows, cols)
	saliency = sc.get_saliency_map(im)
	selection, mask = sc.get_salient_regions_bounded(im)
	
	out = np.concatenate((im, selection), axis=1)

	cv2.imshow("im", out)	
	cv2.imshow("saliency", saliency)	
	cv2.imshow("mask", mask)	
		
	while True:
		key = cv2.waitKey(1) & 255
		if key==27:
			break

	cv2.destroyAllWindows()
